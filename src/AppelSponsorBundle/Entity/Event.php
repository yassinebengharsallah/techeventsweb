<?php

namespace AppelSponsorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="IDX_3BAE0AA7A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="affiche", type="string", length=255, nullable=false)
     */
    private $affiche;

    /**
     * @var string
     *
     * @ORM\Column(name="nbrplaces", type="string", length=255, nullable=false)
     */
    private $nbrplaces;

    /**
     * @var string
     *
     * @ORM\Column(name="localisation", type="string", length=255, nullable=false)
     */
    private $localisation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateevent", type="date", nullable=false)
     */
    private $dateevent;

    /**
     * @var string
     *
     * @ORM\Column(name="hdebut", type="string", length=255, nullable=false)
     */
    private $hdebut;

    /**
     * @var string
     *
     * @ORM\Column(name="hfin", type="string", length=255, nullable=false)
     */
    private $hfin;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=false)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="psilver", type="string", length=255, nullable=false)
     */
    private $psilver;

    /**
     * @var string
     *
     * @ORM\Column(name="pglod", type="string", length=255, nullable=false)
     */
    private $pglod;

    /**
     * @var string
     *
     * @ORM\Column(name="pdiamond", type="string", length=255, nullable=false)
     */
    private $pdiamond;

    /**
     * @var float
     *
     * @ORM\Column(name="prixsilver", type="float", precision=10, scale=0, nullable=false)
     */
    private $prixsilver;

    /**
     * @var float
     *
     * @ORM\Column(name="prixgold", type="float", precision=10, scale=0, nullable=false)
     */
    private $prixgold;

    /**
     * @var float
     *
     * @ORM\Column(name="prixdiamond", type="float", precision=10, scale=0, nullable=false)
     */
    private $prixdiamond;

    /**
     * @var \FosUser
     *
     * @ORM\ManyToOne(targetEntity="FosUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

